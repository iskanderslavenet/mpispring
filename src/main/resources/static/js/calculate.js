const serverAdress = "http://localhost:8080/";

function returnValue(sliderName){
    return document.getElementById(sliderName).value;
}

function calculate(){
    let xhr = new XMLHttpRequest()
    var params = `summ=${returnValue("sliderSumm")}&time=${returnValue("sliderTime")}`;
    console.log(params);
    xhr.open('GET', `${serverAdress}calculate?${params}`, true)
    xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    xhr.send()
    xhr.onreadystatechange = () => {
        if(xhr.readyState != 4) return;
        var ul = document.getElementById("list");
        let day = xhr.response.day;
        let summ = xhr.response.summ;
        console.log(day);
        console.log(summ);
        let finallycalculated = document.getElementById("finallycalculated");
        let summOut = document.getElementById("summOut");
        let timeOut = document.getElementById("timeOut");
        summOut.innerHTML=`Сумма: ${returnValue("sliderSumm")} руб.`;
        timeOut.innerHTML=`Дата: ${returnValue("sliderTime")}`;
        finallycalculated.innerHTML=`Нужно вернуть ${summ} рублей до ${day}`;
    }
}