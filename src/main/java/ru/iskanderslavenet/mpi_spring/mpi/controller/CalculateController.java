package ru.iskanderslavenet.mpi_spring.mpi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.iskanderslavenet.mpi_spring.mpi.domain.LoanInfo;
import ru.iskanderslavenet.mpi_spring.mpi.repos.LoanRepo;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/calculate")
public class CalculateController {

    @Autowired
    LoanRepo loanRepo;

    @PostMapping
    public HashMap<String, String> getSumm(@RequestParam(name = "summ", required = false) String summ,
                                           @RequestParam(name = "time", required = false) String time){
        HashMap<String, String> model = new HashMap<>();
        if(summ != null && !summ.isEmpty() && time != null && !time.isEmpty()){
            int percent = 10; //replace to DB
            int summInt = Integer.parseInt(summ);
            LocalDate localDate = LocalDate.now().plusDays(Integer.parseInt(time));
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.YYYY");
            summInt = summInt + (summInt / 100 * percent) * Integer.parseInt(time);

            model.put("summ", Integer.toString(summInt));
            model.put("day", localDate.format(dtf));
        }
        else{
            model.put("message", "no summ or(and) time found");
        }
        return model;
    }

    @DeleteMapping
    public LoanInfo getAllParams(@ModelAttribute LoanInfo loanInfo){
        return loanInfo;
    }

    @PutMapping
    public String calculateSumm(@RequestParam(name = "SliderSumm", required = true) int summ,
                                @RequestParam(name = "SliderTime", required = true) String time,
                                @RequestParam(name = "FirstName", required = true) String firstName,
                                @RequestParam(name = "LastName", required = true) String lastName,
                                @RequestParam(name = "Surname", required = true) String surname,
                                @RequestParam(name = "Passport", required = true) String passport,
                                @RequestParam(name = "Email", required = true) String email,
                                @RequestParam(name = "Phone", required = true) String phone,
                                @RequestParam(name = "Comment", required = true) String comment){

        //HashMap<String, String> hashMap = new HashMap<>();
        LocalDate localDate = LocalDate.now().plusDays(Integer.parseInt(time));
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.YYYY");

        LoanInfo loan = new LoanInfo(passport, summ, localDate.format(dtf), firstName, lastName, surname, email, phone, comment,
                null, false, null, false);
        try{
            loanRepo.save(loan);
        }
        catch(Exception ex){
            return "error";
        }
        //hashMap.put("stat", "ok");
        return "done";
    }
}