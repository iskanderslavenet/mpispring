package ru.iskanderslavenet.mpi_spring.mpi;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import ru.iskanderslavenet.mpi_spring.mpi.domain.DebtInfo;
import ru.iskanderslavenet.mpi_spring.mpi.domain.LoanInfo;
import ru.iskanderslavenet.mpi_spring.mpi.domain.UserInfo;
import ru.iskanderslavenet.mpi_spring.mpi.repos.DebtRepo;
import ru.iskanderslavenet.mpi_spring.mpi.repos.LoanRepo;
import ru.iskanderslavenet.mpi_spring.mpi.repos.UserInfoRepo;

import javax.annotation.Resource;
import java.time.Duration;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class DatabaseTests {
    private final Duration dbTimeout = Duration.ofSeconds(2);
    String test_username = UUID.randomUUID().toString();
    String collectorName = UUID.randomUUID().toString();

    int duties_count = 5000;
    int loan_count = 5000;

    @Resource
    private UserInfoRepo userInfoRepo;
    @Resource
    private DebtRepo debtRepo;
    @Resource
    private LoanRepo loanRepo;

    @Test
    public void createUserInfoTest() {
        UserInfo user = new UserInfo();
        user.setPassword("123");
        user.setUsername(test_username);

        // check timeout for saving
        assertTimeout(dbTimeout, () -> userInfoRepo.save(user));

        // check saved instance
        UserInfo userDB = assertTimeout(dbTimeout, () -> userInfoRepo.findByUsername(test_username));
        assertEquals(test_username, userDB.getUsername());
    }

    @Test
    public void filterDutiesTest() {
        DebtInfo debt = null;
        for (int i = 0; i < duties_count; i++) {
            debt = new DebtInfo(
                    duties_count + i,
                    "424214412" + i,
                    "123",
                    "firstname",
                    "lastname",
                    "surname",
                    "abc@ml.com",
                    "+79854466852",
                    "comment",
                    "comment",
                    true,
                    collectorName,
                    true
            );
            DebtInfo finalDebt = debt;
            assertTimeout(dbTimeout, () -> debtRepo.save(finalDebt));
        }
        List<DebtInfo> dutiesByCollector = assertTimeout(dbTimeout, () -> debtRepo.findAllByCollectorName(collectorName));
        assertEquals(duties_count, dutiesByCollector.size());
    }

    @Test
    public void filterLoansTest() {
        LoanInfo loan = null;
        for (int i = 0; i < loan_count; i++) {
            loan = new LoanInfo(
                    String.valueOf(loan_count + i),
                    loan_count + i,
                    "123",
                    "firstname",
                    "lastname",
                    "surname", "abc@ml.com",
                    "+79854466852",
                    "comment",
                    "comment",
                    true,
                    collectorName,
                    true
            );
            LoanInfo finalLoan = loan;
            assertTimeout(dbTimeout, () -> loanRepo.save(finalLoan));
        }
        int test_passport = loan_count + loan_count / 2;
        LoanInfo loanByPassport = assertTimeout(dbTimeout, () -> loanRepo.findById(test_passport));
        assertEquals(loanByPassport.getCollectorName(), collectorName);
    }
}
