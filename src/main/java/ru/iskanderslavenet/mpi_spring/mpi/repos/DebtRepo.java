package ru.iskanderslavenet.mpi_spring.mpi.repos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.iskanderslavenet.mpi_spring.mpi.domain.DebtInfo;

import java.util.List;

@Repository
public interface DebtRepo extends CrudRepository<DebtInfo, Long> {
    List<DebtInfo> findAll();
    DebtInfo findById(int id);
    void deleteById(int id);
    List<DebtInfo> findAllByCollectorName(String collectorName);
}
