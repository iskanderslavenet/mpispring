package ru.iskanderslavenet.mpi_spring.mpi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.iskanderslavenet.mpi_spring.mpi.domain.UserInfo;
import ru.iskanderslavenet.mpi_spring.mpi.domain.UserRole;
import ru.iskanderslavenet.mpi_spring.mpi.repos.UserInfoRepo;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

@Controller
@RequestMapping("/registration")
@CrossOrigin
public class RegistrationController {

    @Autowired
    UserInfoRepo userInfoRepo;

    @PostMapping
    @CrossOrigin
    public String registerUser(@RequestParam(name="username", required = true) String username,
                               @RequestParam(name="password", required = true) String password) {
        UserInfo userDB = userInfoRepo.findByUsername(username);
        if(userDB != null) {
            return "FUCK!";
        }
        userDB.setPassword(password);
        userDB.setUsername(username);
        userDB.setActive(true);
        userDB.setRoles(Collections.emptySet());
        userInfoRepo.save(userDB);
        return "password: " + userDB.getUsername();
    }
}
