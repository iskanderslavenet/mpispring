package ru.iskanderslavenet.mpi_spring.mpi.controller;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import ru.iskanderslavenet.mpi_spring.mpi.domain.LoanInfo;
import ru.iskanderslavenet.mpi_spring.mpi.repos.LoanRepo;

import java.util.*;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/loan")
//@PreAuthorize("hasAuthority('MFO')")
public class LoanController {

    @Autowired
    LoanRepo loanRepo;

    @GetMapping("/list")
    public List<LoanInfo> getLoanList(){
        List<LoanInfo> loans = loanRepo.findAll();
        return loans;
    }

    @GetMapping("/list/{id}")
    public LoanInfo getLoanItem(@PathVariable(name = "id") int id){
        LoanInfo loan = loanRepo.findById(id);
        return loan;
    }

    @PostMapping("/list/{id}")
    public HashMap<String, String> approveLoan(@PathVariable(name = "id") int id){
        HashMap<String, String> status = new HashMap<>();
        LoanInfo loan = loanRepo.findById(id);
        loan.setApproved(true);
        loanRepo.save(loan);
        status.put("status", "ok");
        return status;
    }

    @PutMapping("/list/{id}/{comment}")
    public void commentLoan(@PathVariable(name = "id") int id,
                             @PathVariable(name = "comment") String comment){
        LoanInfo loan = loanRepo.findById(id);
        loan.setAdminComment(comment);
        loanRepo.save(loan);
    }

    @DeleteMapping("/list/{id}")
    @Transactional
    public void rejectLoan(@PathVariable(name = "id") int id){
        loanRepo.deleteById(id);
    }
}
