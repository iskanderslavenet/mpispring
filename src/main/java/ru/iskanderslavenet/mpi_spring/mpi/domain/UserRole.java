package ru.iskanderslavenet.mpi_spring.mpi.domain;

public enum UserRole {
    MFO, COLLECTOR;
}
