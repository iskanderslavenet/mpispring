package ru.iskanderslavenet.mpi_spring.mpi.repos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.iskanderslavenet.mpi_spring.mpi.domain.UserInfo;

@Repository
public interface UserInfoRepo extends CrudRepository<UserInfo, Long> {
    UserInfo findByUsername(String username);
    void deleteByUsername(String username);
}
