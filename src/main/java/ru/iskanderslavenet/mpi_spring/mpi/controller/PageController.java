package ru.iskanderslavenet.mpi_spring.mpi.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PageController {
    @GetMapping("/calculate")
    public String calculate(){
        return "calculate.html";
    }

    @GetMapping("/debts")
    public String debts(){
        return "debt.html";
    }

    @GetMapping("/loan")
    public String loan(){
        return "requests.html";
    }

    @GetMapping("/registration")
    public String getRegistration(){
        return "registration.html";
    }

    @GetMapping("/login")
    public String getLogin(){
        return "login.html";
    }
}
