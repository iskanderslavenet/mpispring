package ru.iskanderslavenet.mpi_spring.mpi.repos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.iskanderslavenet.mpi_spring.mpi.domain.LoanInfo;
import ru.iskanderslavenet.mpi_spring.mpi.domain.UserInfo;

import java.util.List;

@Repository
public interface LoanRepo extends CrudRepository<LoanInfo, Long> {
    LoanInfo findById(int passport);
    void deleteById(int id);
    List<LoanInfo> findAllByPassport(int passport);
    List<LoanInfo> findAll();
}