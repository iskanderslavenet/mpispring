package ru.iskanderslavenet.mpi_spring.mpi.controller;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.iskanderslavenet.mpi_spring.mpi.domain.DebtInfo;
import ru.iskanderslavenet.mpi_spring.mpi.repos.DebtRepo;

import java.security.Principal;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Locale;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/debt")
//@PreAuthorize("hasAuthority('COLLECTOR')")
public class DebtController {

    @Autowired
    DebtRepo debtRepo;

    @GetMapping("/all")
    public List<DebtInfo> getDebtList(){
        List<DebtInfo> debts = debtRepo.findAll();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy", Locale.US);
        String localDateString = LocalDate.now().format(dtf);
        LocalDate localDate = LocalDate.parse(localDateString, dtf);
        for(DebtInfo debt : debts){
            LocalDate debtDate = LocalDate.parse(debt.getTime(), dtf);
            long days = ChronoUnit.DAYS.between(debtDate, localDate);
            debt.setTime(Long.toString(days));
        }
        return debts;
    }

    @GetMapping("/list")
    public List<DebtInfo> getLocalDebtList(Principal principal){
        String collectorName = principal.getName();
        List<DebtInfo> debtInfos = debtRepo.findAllByCollectorName(collectorName);
        return debtInfos;
    }

    @DeleteMapping("/list/{id}")
    public DebtInfo deleteDebt(Principal principal, @PathVariable(name = "id") int id){
        String collectorName = principal.getName();
        List<DebtInfo> debtInfos = debtRepo.findAllByCollectorName(collectorName);
        DebtInfo info = new DebtInfo();
        for (DebtInfo d: debtInfos) {
            if(d.getCollectorName() == collectorName && d.getId() == id)
                info = d;
        }
        return info;
    }

    @PostMapping("/list/{id}")
    public DebtInfo getDebtInfo(@PathVariable(name = "id") int id){
        //String collectorName = principal.getName();
        DebtInfo debtInfos = debtRepo.findById(id);
        return debtInfos;
    }

    @PutMapping("/list/{id}")
    public void takeDebt(Principal principal, @PathVariable(name = "id") int id){
        String collectorName = principal.getName();
        DebtInfo debt = debtRepo.findById(id);
        debt.setCollectorName(collectorName);
        debt.setDebt(true);
        debtRepo.save(debt);
    }
}