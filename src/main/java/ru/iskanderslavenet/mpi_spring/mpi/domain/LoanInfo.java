package ru.iskanderslavenet.mpi_spring.mpi.domain;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;

@Entity
@Table(name = "loan")
public class LoanInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Pattern(regexp = "[0-9]{10}")
    private String passport;
    private int summ;
    private String time;
    @Pattern(regexp = "[А-Я]]")
    private String firstName;
    @Pattern(regexp = "[А-Я]]")
    private String lastName;
    @Pattern(regexp = "[А-Я]]")
    private String surname;
    @Email
    private String email;
    @Pattern(regexp = "\\+7[0-9]{10}")
    private String phone;
    private String comment;
    private String adminComment;
    private boolean isDebt;
    private String collectorName;
    private boolean isApproved;

    public int getId() {
        return id;
    }
    
    public void foo(){};

    public void setId(int id) {
        this.id = id;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public int getSumm() {
        return summ;
    }

    public void setSumm(int summ) {
        this.summ = summ;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getAdminComment() {
        return adminComment;
    }

    public void setAdminComment(String adminComment) {
        this.adminComment = adminComment;
    }

    public boolean isDebt() {
        return isDebt;
    }

    public void setDebt(boolean debt) {
        isDebt = debt;
    }

    public String getCollectorName() {
        return collectorName;
    }

    public void setCollectorName(String collectorName) {
        this.collectorName = collectorName;
    }

    public boolean isApproved() {
        return isApproved;
    }

    public void setApproved(boolean approved) {
        isApproved = approved;
    }

    public LoanInfo() {}

    public LoanInfo(String passport, int summ, String time, String firstName, String lastName, String surname, String email, String phone, String comment, String adminComment, boolean isDebt, String collectorName, boolean isApproved) {
        this.passport = passport;
        this.summ = summ;
        this.time = time;
        this.firstName = firstName;
        this.lastName = lastName;
        this.surname = surname;
        this.email = email;
        this.phone = phone;
        this.comment = comment;
        this.adminComment = adminComment;
        this.isDebt = isDebt;
        this.collectorName = collectorName;
        this.isApproved = isApproved;
    }
}
